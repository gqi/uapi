## Submit Hadoop Jobs from LSF

#### Set related LSF job group size
If the LSF job group size set to 1, you can submit all the jobs but only 1 will be run at the time.

`cd ../common`  

`./lsf-group-info-reset submit -type UAPI-hadoop-solr-document -size 1`  
`./lsf-group-info-reset list -type UAPI-hadoop-solr-document` 

`./lsf-group-info-reset submit -type UAPI-hadoop-solr -size 1`  
`./lsf-group-info-reset list -type UAPI-hadoop-solr`  

`./lsf-group-info-reset submit -type UAPI-hadoop-voldemort -size 1`  
`./lsf-group-info-reset list -type UAPI-hadoop-voldemort`  

#### Submit Solr documents creation or Solr indexing jobs
`cd documents`  or `cd solr`  

`../submit-hadoop-job all 2020_06`  
`../submit-hadoop-job uniprot,uniref,uniparc,suggest,genecentric 2020_06`

`../submit-hadoop-job all 2020_06 submit`  
`../submit-hadoop-job uniprot,uniref,uniparc,suggest,genecentric 2020_06 submit`

#### Submit voldemort jobs
`cd voldemort` 

`../submit-hadoop-job all 2020_06`  
`../submit-hadoop-job uniprot,uniref-ligth,uniref-member,uniparc 2020_06`

`../submit-hadoop-job all 2020_06 submit`  
`../submit-hadoop-job uniprot,uniref,uniparc,suggest,genecentric 2020_06 submit`