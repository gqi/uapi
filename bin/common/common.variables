#!/bin/bash

# ---------------------------- VALIDATE USER ----------------------------
# require VALID_USERS defined
if [ -z $VALID_USERS ]; then
  echo "VALID_USERS array variable must be defined, e.g., VALID_USERS=(\"uni_adm\");"
  exit 1
fi

i_am_a_valid_user="no"
user=$(whoami)
for valid_user in "${VALID_USERS[@]}"; do
  if [ "$user" == "$valid_user" ]; then
    i_am_a_valid_user="yes"
  fi
done

if [ "$i_am_a_valid_user" == "no" ]; then
  echo "You ($user) are not allowed to run this script. Authorized users are: ${VALID_USERS[@]}."
  exit 1
fi

if [ -z "$RELEASE_NUMBER" ]; then
  RELEASE_NUMBER="$private_release"
fi

if [ -z "$RELEASE_NUMBER" ]; then
  echo "Release number could not be found in $GIT_RO/src/sh/sp_wr/variables.sh"
  exit 1
fi

# ---------------------------- ENVIRONMENT ----------------------------

# specify here because of problems with LSF choosing a different version for different users
rsync="/usr/bin/rsync"

## ---------------------------- LSF ----------------------------

# who to email when LSF jobs finish; comma separated format on a single line
#LSF_EMAIL="eddturner,rantunes,wudong,jluo,gqi,$LOGNAME"
LSF_EMAIL="$LOGNAME"
LSF_CORES=1,2
LSF_Q=production-rh74
LSF_MEM=5120
LSF_GROUP_NAME="/UAPI-$TASK"

## ---------------------------- GENERAL UTILITY FUNCTIONS ----------------------------
# quick way for echoing coloured text
# e.g., prettyEcho "<warn>warning message</warn>"
# e.g., prettyEcho "<info>info message: <warn>specific message</warn></info>"
# e.g., prettyEcho "<error>warning message</error>"
function prettyEcho() {
  string="$1"
  string=$(echo $string | perl -pe 's@<warn>(.*?)<\/warn>@\$(tput setaf 3)\1\$(tput sgr 0)@g')
  string=$(echo $string | perl -pe 's@<info>(.*?)<\/info>@\$(tput setaf 2)\1\$(tput sgr 0)@g')
  string=$(echo $string | perl -pe 's@<error>(.*?)<\/error>@\$(tput setaf 1)\$(tput bold)\1\$(tput sgr 0)@g')
  string=$(echo $string | perl -pe 's@<success>(.*?)<\/success>@\$(tput setaf 2)\$(tput bold)\1\$(tput sgr 0)@g')
  string=$(echo $string | perl -pe 's@<bold>(.*?)<\/bold>@\$(tput bold)\1\$(tput sgr 0)@g')
  eval "echo "$string""
}

function printDivider() {
  if [ ! -z "$1" ]; then
    title=" $1 "
  else
    title=""
  fi
  echo "===========================$title==========================="
}
