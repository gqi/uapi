#!/bin/bash
# create a new collection on one solr cluster with number of shards and replicates
set -euo pipefail
IFS=$'\n\t'

if [ $USER != "uni_adm" ]; then
  echo "This service can only be run with user 'uni_adm'"
  exit 1
fi

if [ $# -ne 3 ]; then
  echo "Usage example: ../../create-collection collection num_shards num_replicates"
  exit 1
fi

COLLECTION=$1
SHARD=$2
REPL=$3

BASE_DIR=$(cd $(dirname $0) && pwd)
source $BASE_DIR/solr-common.variables

SOLR_VM_LIST=("${SOLR_VMS[@]}")
eval $(ssh "${SOLR_VM_LIST[0]}" "cat $SOLR_ROOT/cluster.properties | grep -v '# '")

curl --user ${SOLR_USER}:${SOLR_PASS} "${SOLR_VM_LIST[0]}:$SOLR_PORT/solr/admin/collections?action=CREATE&name=${COLLECTION}&numShards=${SHARD}&replicationFactor=${REPL}&maxShardsPerNode=10&collection.configName=${COLLECTION}" && {
  echo "The collection ${COLLECTION} is created on the Solr with ${SHARD} shards and ${REPL} replicates"
}
