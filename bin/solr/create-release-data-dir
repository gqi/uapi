#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# create directory layout in the data directory for the current release.
# copy solr_home and ujdk_lib from release directory to the created directory layout.

if [ $USER != "uni_adm" ]; then
  echo "This service can only be run with user 'uni_adm'"
  exit 1
fi

if [ $# -ne 2 ]; then
  echo 'USAGE: create-release-data-dir release_dir num_nodes'
  echo 'This script only work on local clusters.'
  exit 1
fi

RELEASE_DIR=$1
NUM_NODES=$2

SOLR_DATA_ROOT=$(cd $(dirname $0)/../../../ && pwd)
SOLR_DATA_RELEASE_DIR=$SOLR_DATA_ROOT/production_data/solr_index_data/$RELEASE_DIR

if [ -e $SOLR_DATA_RELEASE_DIR ]; then
  echo "release directory already existed: $SOLR_DATA_RELEASE_DIR"
  exit 1
fi

echo "creating the data release directory layout"
mkdir -p $SOLR_DATA_RELEASE_DIR

SOLR_DATA=$SOLR_DATA_RELEASE_DIR/solr_data
mkdir -p $SOLR_DATA
echo "$SOLR_DATA created"

for i in $(seq 1 $NUM_NODES); do
  mkdir -p $SOLR_DATA/node_${i}_data
done
echo "data directory for each node created"

mkdir -p $SOLR_DATA_RELEASE_DIR/logs
mkdir -p $SOLR_DATA_RELEASE_DIR/stats
mkdir -p $SOLR_DATA_RELEASE_DIR/ujdk_lib

echo "$SOLR_DATA_RELEASE_DIR layout created:"
tree $SOLR_DATA_RELEASE_DIR

GIT_REPO=$SOLR_DATA_ROOT/git_repo/uniprot-store/index-config/src/main/solr-config/uniprot-collections/
echo "Copying Solr config files for each collection from $GIT_REPO"
echo "to the release directory $SOLR_DATA_RELEASE_DIR/solr_config"
cp -r $GIT_REPO $SOLR_DATA_RELEASE_DIR/solr_config

echo "Done"